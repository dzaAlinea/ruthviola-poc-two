﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
   
    public Transform targetObject;
    public AnimationCurve animCurve;

    private Transform rotatorContainer;

    private float speed = 100;
    private float verticalDir = -20;
    private float curVerticalSpeed;
    private float curHorizontalSpeed;
    private Vector2 curMousePos;
    private bool CanRotate = false;
    void Start()
    {
        rotatorContainer = new GameObject("RotatorContainer").transform;
        rotatorContainer.transform.position = Vector3.zero;
        rotatorContainer.transform.eulerAngles = new Vector3(verticalDir, 0, 0);
        CanRotate = false;
    }


    public void ResetRotation()
    {
        curVerticalSpeed = 0;
        curHorizontalSpeed = 0;
        rotatorContainer.eulerAngles = new Vector3(verticalDir, 0, 0);
    }


    public void SetRotateObject(GameObject obj)
    {
        ResetRotation();
        targetObject = obj.transform;
        

        if (targetObject != null)
        {
            StartCoroutine("MovingObjectToCenter");
        }
    }

    public void RemoveRotateObject()
    {
        targetObject.SetParent(null);
        targetObject = null;
        CanRotate = false;
    }

    private IEnumerator MovingObjectToCenter()
    {
        CanRotate = false;
        targetObject.transform.SetParent(rotatorContainer);
        Vector3 startPos = targetObject.transform.localPosition;
        Quaternion startRot = targetObject.transform.localRotation;
        float startTime = Time.time;
        float speed = 2;
        while ((Time.time - startTime) * speed <= 1)
        {
            
            targetObject.localPosition = Vector3.Lerp(startPos, Vector3.zero, animCurve.Evaluate((Time.time - startTime) * speed));
            targetObject.localRotation = Quaternion.Slerp(startRot, Quaternion.identity, animCurve.Evaluate((Time.time - startTime) * speed));
            yield return null;
        }

        targetObject.localPosition = rotatorContainer.position;
        targetObject.localRotation = Quaternion.identity;
        
        CanRotate = true;
    }


    // Update is called once per frame
    void LateUpdate()
    {
        if (targetObject == null || !CanRotate) return;

        if (Input.GetMouseButtonDown(0))
        {
            curMousePos = Input.mousePosition;
        }
        if (Input.GetMouseButton(0))
        {
            //Horizontal rotation
            curHorizontalSpeed = (speed / (Screen.width / 100f)) * (Input.mousePosition.x - curMousePos.x) * Time.deltaTime;


            //Vertical Rotation
            curVerticalSpeed = (speed / (Screen.height / 100f)) * (Input.mousePosition.y - curMousePos.y) * Time.deltaTime;
            verticalDir += curVerticalSpeed;
            curMousePos = Input.mousePosition;
        }
        else
        {
            curHorizontalSpeed = Mathf.Lerp(curHorizontalSpeed, 0, Time.deltaTime * 10);
            curVerticalSpeed = Mathf.Lerp(curVerticalSpeed, 0, Time.deltaTime * 10);
            verticalDir += curVerticalSpeed;


        }

        verticalDir = Mathf.Max(Mathf.Min(verticalDir, 70), -70);
        targetObject.transform.localEulerAngles -= new Vector3(0, curHorizontalSpeed, 0);
        rotatorContainer.transform.eulerAngles = new Vector3(verticalDir, 0, 0);
    }
}
