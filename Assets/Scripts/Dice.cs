﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class Dice : MonoBehaviour
{
    public Vector3 startPosition;
    public TextMeshPro[] textMesh;
    public Task task;

    void Start()
    {
        startPosition = transform.position;
        ActivateDiceColliders(false);
    }

    public void SetLetters()
    {
        List<int> index = new List<int>();
        for (int i = 0; i < textMesh.Length; i++)
        {
            index.Add(i);
        }


        for (int i = 0; i < textMesh.Length; i ++)
        {
            int randomIndex = UnityEngine.Random.Range(0, index.Count);
            textMesh[i].text = task.chars[index[randomIndex]];
            index.Remove(index[randomIndex]);
        }

    }

    public void ActivateDiceColliders(bool activate)
    {
        foreach (TextMeshPro mesh in textMesh)
        {
            mesh.gameObject.GetComponentInChildren<BoxCollider>().enabled = activate;
        }
    }
}
