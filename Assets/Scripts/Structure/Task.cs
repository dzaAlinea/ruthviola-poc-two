﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class Task
{
    public Sprite taskImg;
    public string[] chars;
    public string text;
    public AudioClip clip;
}
