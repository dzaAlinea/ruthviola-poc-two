﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
public class DiceGameManager : MonoBehaviour
{
    public Text text;
    public Image image;
    public Rotator rotator;
    public Dice[] dices;
    public Animator background;
    public Animator ruthviolaAnim;
    public Animator speachBubbleAnim;
    public Animator textAnimator;
    public Animator imageAnimator;
    public Animator starAnim;
    public AudioSource audioSource;
    public Transform[] pointPositions;
    public AudioClip introClip;
    public AudioClip[] wrongClips;
    public AudioClip[] rightClips;
    public AudioClip wonClip;
    public AudioClip lostClip;
    public int points;

    private DiceGameState curGameState;
    private Dice curDice;

    private float mouseDownTime;


    private void Start()
    {
        background.SetBool("FadeIn", false);
        ChangeGameState(DiceGameState.Intro);
        for (int i = 0; i < dices.Length; i ++)
        {
            Debug.Log(i);
            dices[i].task = GameData.ins.tasks[i];
            dices[i].SetLetters();
        }
    }


    public void ChangeGameState(DiceGameState gameState)
    {
        StartCoroutine("ChangingGameState", gameState);
    }

    private IEnumerator ChangingGameState(DiceGameState gameState)
    {
        Debug.Log("Changing gamestate to " + gameState);
        curGameState = gameState;
        yield return null;

        switch (gameState)
        {
            case DiceGameState.Intro:
                yield return new WaitForSeconds(2);
                audioSource.clip = introClip;
                audioSource.Play();
                ChangeGameState(DiceGameState.PickDice);
                break;
            case DiceGameState.PickDice:
                
                yield return new WaitForSeconds(0.5f);
                speachBubbleAnim.SetBool("Open", false);
                yield return new WaitForSeconds(0.5f);
                background.SetBool("FadeIn", false);
                ruthviolaAnim.SetBool("Open", false);
                textAnimator.SetBool("FadeIn", false);
                imageAnimator.SetBool("FadeIn", true);


                break;
            case DiceGameState.ChooseLetter:
                rotator.SetRotateObject(curDice.gameObject);
                image.sprite = curDice.task.taskImg;
                image.SetNativeSize();
                text.text = curDice.task.text;
                background.SetBool("FadeIn", true);
                yield return new WaitForSeconds(1f);
                ruthviolaAnim.SetBool("Open", true);
                yield return new WaitForSeconds(0.8f);
                audioSource.clip = curDice.task.clip;
                audioSource.Play();
                speachBubbleAnim.SetBool("Open", true);
                curDice.ActivateDiceColliders(true);
                break;
            case DiceGameState.Answered:

                break;
            case DiceGameState.Win:
                starAnim.SetTrigger("GotStar");
                audioSource.clip = wonClip;
                audioSource.Play();
                break;
            case DiceGameState.Lose:

                break;
        }
    }


    private void AnswerQuestion(string answer)
    {
        curDice.GetComponent<Animator>().SetTrigger("Click");

        if (answer == curDice.task.chars[0])//Right
        {
            audioSource.clip = rightClips[Random.Range(0, rightClips.Length)];
            audioSource.Play();
            StartCoroutine(AnimatingToPointPosition());
        }
        else//Wrong
        {
            audioSource.clip = wrongClips[Random.Range(0, wrongClips.Length)];
            audioSource.Play();
        }
    }

    private IEnumerator AnimatingToPointPosition()
    {
        
        textAnimator.SetBool("FadeIn", true);
        imageAnimator.SetBool("FadeIn", false);

        yield return new WaitForSeconds(2);

        DestroyImmediate(curDice.gameObject.GetComponent<Animator>());
        rotator.RemoveRotateObject();

        Vector3 startPos = curDice.transform.position;
        Quaternion startRot = curDice.transform.rotation;
        float startTime = Time.time;
        float speed = 2;
        while ((Time.time - startTime) * speed <= 1)
        {

            curDice.transform.position = Vector3.Lerp(startPos, pointPositions[points].position + new Vector3(0,0,-0.1f), rotator.animCurve.Evaluate((Time.time - startTime) * speed));
            curDice.transform.localScale = Vector3.Lerp(Vector3.one, Vector3.one * 0.12f, rotator.animCurve.Evaluate((Time.time - startTime) * speed));
            yield return null;
        }

        curDice.transform.position = pointPositions[points].position + new Vector3(0, 0, -0.1f);
        curDice.transform.localScale = Vector3.one * 0.12f;

        points++;

        if (points < 10)
        {
            ChangeGameState(DiceGameState.PickDice);
        }
        else
        {
            ChangeGameState(DiceGameState.Win);
        }



    }


    private void ApplyPoint()
    {

    }


    private void Update()
    {
        if (curGameState == DiceGameState.PickDice) {
            if (Input.GetMouseButtonUp(0))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, 100))
                {
                    curDice = hit.transform.GetComponent<Dice>();
                    if (curDice != null)
                    {
                        ChangeGameState(DiceGameState.ChooseLetter);
                    }
                }
            }
        }else if (curGameState == DiceGameState.ChooseLetter)
        {
            if (Input.GetMouseButtonDown(0))
            {
                mouseDownTime = Time.time;
            }
            if (Input.GetMouseButtonUp(0) && Time.time - mouseDownTime < 0.2f)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, 100))
                {
                    TextMeshPro text = hit.transform.GetComponentInParent<TextMeshPro>();
                    if (text != null)
                    {
                        AnswerQuestion(text.text);
                    }
                }
            }
        }
    }




}


public enum DiceGameState
{
    Intro,
    PickDice,
    ChooseLetter,
    Answered,
    Win,
    Lose
    

}